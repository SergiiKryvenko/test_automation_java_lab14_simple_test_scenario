package com.epam.internal.course.SimpleTestScenario.ui.tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.epam.internal.course.SimpleTestScenario.ui.data.DataRepository;
import com.epam.internal.course.SimpleTestScenario.ui.data.SearchData;
import com.epam.internal.course.SimpleTestScenario.ui.page.MainGooglePage;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class SimpleTest extends TestRunner {

    @DataProvider
    public Object[][] searchText() {
        return new Object[][] { { DataRepository.get().searchText("Apple") }, };
    }

    /**
     * A simple test using Selenium. In Google page, we search request which consists of a random string.
     * @param word Random string (20 letters).
     */
    @Epic("A simple test using Selenium.")
    @Feature(value = "search a text 'Apple'")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("In Google page, we search request which consists of a text 'Apple'.")
    @Story(value = "Make a search request with a text 'Apple'. Check if we are in Images tab.")
    @Parameters("Text")
    @Test(dataProvider = "searchText")
    public void search(SearchData text) {
        logger.info("start search with " + text.getSearchText());
        MainGooglePage mainGooglePage = new MainGooglePage(driver).searchText(text.getSearchText());
        Assert.assertEquals(mainGooglePage.getTitle().contains(text.getSearchText()), true, "You found a wrong result");
        Assert.assertTrue(mainGooglePage.goToImagesTab().isImagesMenuTabDisplayed(), "test is failed");
    }
}
